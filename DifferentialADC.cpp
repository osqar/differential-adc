#include "DifferentialADC.h"

//ADC Differential Properties
byte ReferenceMask=DEFAULT;
short int PrescalerSelector=PRESCALER128; //case 0...6 or DivisionFactorValue: 2,4,8,16,32,64,128


/*			TABLE 1
	16-29			ADC+		ADC-		Gain	OccupiedChannel-Bits (Counting from 0)
Channel Option											
	16				0			1			1x				1

	18				2			1			1x				2
	19				3			1			1x				3
	20				4			1			1x				4
	21				5			1			1x				5
	22				6			1			1x				6
	23				7			1			1x				7


	48-61			ADC+		ADC-		Gain
	48				8			9			1x				8

	50				10			9			1x				9
	51				11			9			1x				10
	52				12			9			1x				11
	53				13			9			1x				12
	54				14			9			1x				13
	55				15			9			1x				14
	
	ADC Manager will map ChannelOption to Channel Value (16 channeloptions to 64 configuration posibilities)
	
*/

//Designed only for Differential Channels (Check Table 1)
byte MapOptionToRegister(byte SelectedOption) //from 1-61 to 0-16
{
	if(SelectedOption==16)
	{
		return 1;
	}
	if(SelectedOption==48)
	{
		return 8;
	}
	if(SelectedOption<1||SelectedOption>61)
		return 0;
	else
	{
		byte Register=SelectedOption%8;
		if(SelectedOption<48)
			return Register;
		else
			return Register+7;
	}
}

byte MapRegisterToOption(byte ShiftMask) //from 0-16 to 1-61
{
	if(ShiftMask==1)
	{
		return 16;
	}
	if(ShiftMask==8)
	{
		return 48;
	}
	if(ShiftMask<1||ShiftMask>16)
		return 0;
	else
	{
		if(ShiftMask<8)
			return 16+ShiftMask;
		else
			return 41+ShiftMask;
	}
}


/**********************************************************************
*					ADCManager CLASS
**********************************************************************/

void ADCManager::Initialize()
{
	OccupiedChannels=0;
	ConversionReadyRegister=0;
	PendingConversionsRegister=0;
	Conversion=0;
	Option=0;
	PendingConversions.Initialize();
	this->Setup();
}

void ADCManager::Runner()
{
	this->ReadConversion();
}


void ADCManager::Setup() //first time setup
{
	/**************************************************************
					Differential ADC Setup
	***************************************************************/
	/**************************************************************
	Single Conversion
		*Disable ADC Power Reduction bit on Power Reduction Register
		*Make sure ADATE and ADSC are clear
		*Select reference
			-AVCC (not more than +/-0.3V from VCC
			-Internal
				.1.1V
				.2.56V
			-External (Using a low value resitence between external
						source and AREF pin it's recommended)
		*Select channel
		*Select Prescaler
		*Set ADC Enable bit (ADEN)
	***************************************************************/
	cbi(PRR0,PRADC); //Disable ADC power reduction
	cbi(ADCSRA,ADEN); //Disable ADC
	cbi(ADCSRA,ADATE); //Disable Auto-Trigger
	cbi(ADCSRA,ADSC); //Stop conversion

	//Select reference
	ReferenceMask=DEFAULT<<6; //Value B01000000
	
	//Select channel & Gain
	ADMUX=ReferenceMask|(Option&(31)); //ADLAR 0 + selected options
	if(Option&32)
	sbi(ADCSRB,3); //Set MUX5
	else
	cbi(ADCSRB,3); //Else clear
	//Then it must be decode what Channel and Gain has been selected (State machine)
	/*****************************************************************************/
	
	//Select Prescaler
	ADCSRA=B00000000; //Clear ADCSRA
	byte PrescalerSelectorBits=0;
	if(!(PrescalerSelector%2)&&PrescalerSelector<=128)
	{
		while(PrescalerSelector>1)
		{
			PrescalerSelector=PrescalerSelector/2;
			PrescalerSelectorBits=PrescalerSelectorBits+1;
		}
	}
	else
	{
		switch (PrescalerSelector)
		{
			case PRESCALER2: //2
				PrescalerSelectorBits=1;
			break;
			case PRESCALER4: //4
				PrescalerSelectorBits=2;
			break;
			case PRESCALER8: //8
				PrescalerSelectorBits=3;
			break;
			case PRESCALER16: //16
				PrescalerSelectorBits=4;
			break;
			case PRESCALER32: //32
				PrescalerSelectorBits=5;
			break;
			case PRESCALER64: //64
				PrescalerSelectorBits=6;
			break;
			case PRESCALER128: //128
				PrescalerSelectorBits=7;
			break;
			default: //128
				PrescalerSelectorBits=8;
		}
	}
	
	//Update ADCSRA value
	ADCSRA=ADCSRA|PrescalerSelectorBits;
	
	//Enable ADC and clear ADC Flag
	ADCSRA=ADCSRA|144;
}

int ADCManager::ReadConversion()
{
	//ADC Single Conversion mode: Read Conversion
	/*
		If Single Conversion mode is selected, ADSC must
		be set to start conversion, and will remain set until the
		conversion has finished.
		(First Conversion takes 25 ADC Clock cycles after ADEN is set.
		The following conversions will take 13 ADC Clock cycles)
		
		*Check if ADSC is High (Or ADIF)
		*When ADSC is Low, set ADSC
	*/
	
	//Start Conversion
	if(!this->isConversionRunning())
	{	
		Conversion=ReadRegisters();  //Read ADCL and ADCH
		sbi(ADCSRA,ADIF); //Clear Interrupt Flag
		
		this->ChannelManager(); //Channel Select before next conversion starts
		
		sbi(ADCSRA,ADSC); //Set ADSC (Start Conversion)
		
	}
	return Conversion;
}

short int ADCManager::ReadRegisters()
{
	/*
		ADC Differential Read Register: When ADLAR is cleared
		
		When a conversion's finished, the result can be read
		in ADCH and ADCL registers.
		
		Concatenation of both registers give a 10-bit resolution
		number, in Two's commplement representation.
		
		Range: -512d   -   512d
		
		Example:
		
		B00 0000 0010 = 2d
		
		Now for a two's complement number:
		B11 1111 1110 => 10th bit is set, that means is a negative number.
		The procedure to read a two's complement number is
		B11 1111 1110 - 1 = ~B11 1111 1101 = B00 0000 0010 = -2d
	*/
	short int ADCRegister=0;
	byte LowByte=ADCL; //ADC Data Register Blocked
	byte HighByte=ADCH; //ADC Data Register Refreshed
	
	ADCRegister=(HighByte<<8|LowByte);
	
	if(ADCRegister&65024) //Negative number. 6524d = B1111 1110 0000 0000
	return -(~(ADCRegister-1)); //Two's Complement
	else //Positive number
	return ADCRegister;
}

bool ADCManager::isConversionRunning()
{
	if(ADCSRA&64)
	return TRUE;
	else
	return FALSE;
}

/*************************************************************************
*
*
*							Subscriber Methods				
*
*
*************************************************************************/

void ADCManager::Subscribe(DifferentialADC * Channel)
{
	byte BufferIndex=MapOptionToRegister(Channel->ChannelOption); //returns buffer index
	
	//Clear ConversionReady Flag
	ConversionReadyRegister=ConversionReadyRegister&(~(1<<BufferIndex));
	ConversionBuffer[BufferIndex]=Channel;
	
	//Sets OccupiedChannels Register
	OccupiedChannels=OccupiedChannels|(1<<BufferIndex);
	BufferIndex=PendingConversionsRegister&(~(1<<BufferIndex));

	//Clear Pending Request Flag
	this->ConversionAttended(Channel->ChannelOption);
	
	//Enqueu Request (no more than 16 Channels)
	PendingConversions.Enqueu(Channel->ChannelOption);
	this->SetConversionPending(Channel->ChannelOption);
}

void ADCManager::UnSubscribe(DifferentialADC * Channel)
{
	byte BufferIndex=MapOptionToRegister(Channel->ChannelOption); //returns buffer index	
	
	//Release Channel
	OccupiedChannels=OccupiedChannels&(~(1<<BufferIndex));
	
	//Pointer to NULL
	ConversionBuffer[BufferIndex]=0;
	
	//Clear pending request flag
	this->ConversionAttended(Channel->ChannelOption);
}

bool ADCManager::isChannelSubscribed(byte ChannelOption)
{
	byte ShiftMask=MapOptionToRegister(ChannelOption);
	ShiftMask=OccupiedChannels&(1<<ShiftMask);
	if(ShiftMask)
		return TRUE;
	else
		return FALSE;
}

/*******************************************************
*	ConversionManager State Machine:
*	
*	0.Subscribe (and Enqueu)
*								 _		  _ _ _
*	1.DeQueu Request			|_|  <-  |_|_|_|
*	2.Change Channel
*	3.Update Corresponding ChannelConversion Value
*	4.Read Conversion
*							 _ _ _      _
*	4.Re-Enqueu Request		|_|_|_| <- |_|
*	5. Re-Start Cicle (from 1)
*******************************************************/

void ADCManager::ChannelManager()
{
	if(this->isChannelSubscribed(Option))
	{
		this->ConversionAttended(Option);			
		//Update Current Conversion
		this->UpdateConversionValue(Option);
		//Re-Enqueu if subscribed
		this->ConversionRequest(Option);
	}		
	//Dequeu Next Conversion Option
	this->AttendNextConversion();	
}

void ADCManager::UpdateConversionValue(byte ChannelOption)
{
	this->SetConversionReadyFlag(ChannelOption);
	
	byte BufferIndex=MapOptionToRegister(ChannelOption);
	if(this->isChannelSubscribed(ChannelOption)) //This can't be pointing null
	{
		ConversionBuffer[BufferIndex]->ChannelConversion=Conversion;
		/*For OsqarProtocol*/
		ConversionBuffer[BufferIndex]->UpdateConversionBytesValues();
		/**/
	}
}

void ADCManager::AttendNextConversion()
{
	this->SetConversionReadyFlag(Option);
	//Update Option Value
	Option=0;
	bool End=FALSE;
	do
	{
		this->ClearConversionPending(Option);
		Option=PendingConversions.Dequeu();
		this->SetConversionPending(Option);
		End=(this->isChannelSubscribed(Option))|(~PendingConversions.MessagesInQueu()); //ERROR if Messages in Queu is FALSE
	}while(!End);
	
	this->ClearConversionReadyFlag(Option);
	
	//Change Channel
	//Update ADMUX (Channel option)
	//Select channel & Gain
	ADMUX=ReferenceMask|(Option&(31)); //ADLAR 0 + selected options
	if(Option&32)
		sbi(ADCSRB,3); //Set MUX5
	else
		cbi(ADCSRB,3); //Else clear		
}

//DifferentialADC MUST BE SUBSCRIBED
void ADCManager::ConversionRequest(byte ChannelOption)
{
		PendingConversions.Enqueu(ChannelOption);
		this->SetConversionPending(ChannelOption);
}

void ADCManager::ConversionAttended(byte ChannelOption)
{
	//Clears Pending Conversion Flag
	this->ClearConversionPending(ChannelOption);	
}

void ADCManager::SetConversionPending(byte ChannelOption)
{
	//Sets PendingConversionFlag
	byte ShiftMask=MapOptionToRegister(ChannelOption);
	ShiftMask=PendingConversionsRegister|(1<<ShiftMask);	
}

void ADCManager::ClearConversionPending(byte ChannelOption)
{
	//Clears Pending Conversion Flag
	byte ShiftMask=MapOptionToRegister(ChannelOption);
	ShiftMask=PendingConversionsRegister&(~(1<<ShiftMask));
}

bool ADCManager::isConversionPending(byte ChannelOption)
{
	//Check PendingConversionFlag
	byte ShiftMask=MapOptionToRegister(ChannelOption);
	ShiftMask=PendingConversionsRegister&(1<<ShiftMask);
	if(!ShiftMask)
		return TRUE;
	else
		return FALSE;
}

void ADCManager::SetConversionReadyFlag(byte ChannelOption)
{
	byte ShiftMask=MapOptionToRegister(ChannelOption);
	ConversionReadyRegister=ConversionReadyRegister|(1<<ShiftMask);
}

void ADCManager::ClearConversionReadyFlag(byte ChannelOption)
{
	byte ShiftMask=MapOptionToRegister(ChannelOption);
	ConversionReadyRegister=ConversionReadyRegister&(~(1<<ShiftMask));
}

/**********************************************************************
*					DifferentialADC CLASS
**********************************************************************/

//SelectedOption (1:61)
byte DifferentialADC::Attach(byte SelectedOption, ADCManager * ConversionManager)
{
	ADManager=ConversionManager;
	if(this->isChannelAvailable(SelectedOption))
	{
		ChannelOption=SelectedOption;
		ADManager->Subscribe(this);
		ChannelConversion=0;
		return ChannelOption;
	}
	else
	{
		ChannelStatus=255;
		return ChannelStatus;
	}
}

byte DifferentialADC::Attach(ADCManager * ConversionManager)
{
	ADManager=ConversionManager;
	ChannelOption=this->SelectAvailableChannel();
	if(ChannelStatus<255)
	{
		ADManager->Subscribe(this);
		ChannelConversion=0;
		return ChannelOption;
	}
	else
		return 255; //error
}

void DifferentialADC::Detach()
{
	ADManager->UnSubscribe(this);
}

/*
	16-29			ADC+		ADC-		Gain	OccupiedChannel-Bits (Counting from 0)
	16				0			1			1x				1

	18				2			1			1x				2
	19				3			1			1x				3
	20				4			1			1x				4
	21				5			1			1x				5
	22				6			1			1x				6
	23				7			1			1x				7


	48-61			ADC+		ADC-		Gain
	48				8			9			1x				8

	50				10			9			1x				9
	51				11			9			1x				10
	52				12			9			1x				11
	53				13			9			1x				12
	54				14			9			1x				13
	55				15			9			1x				14
	
*/

bool DifferentialADC::isChannelAvailable(byte SelectedOption) //from 1-61
{
	byte ShiftRegisterBits=MapOptionToRegister(SelectedOption);
	if(ShiftRegisterBits<16)
	{
		if(ADManager->OccupiedChannels&(1<<(ShiftRegisterBits)))
			return FALSE;
		else
			return TRUE;
	}
	else
		return FALSE;
}

byte DifferentialADC::SelectAvailableChannel()
{
	byte ShiftMask=1;
	while(ShiftMask<16)
	{
		byte Mask=ADManager->OccupiedChannels&(1<<ShiftMask);
		if(Mask==0)
		{
			ChannelOption=MapRegisterToOption(ShiftMask);
			return ChannelOption;
		}
		ShiftMask++;
	}
	return 255;//error
}

long DifferentialADC::ReadConversion()
{
	return ChannelConversion;
}

bool DifferentialADC::isConversionAvailable()
{
	byte OptionShiftRegister=MapOptionToRegister(ChannelOption);
	//Check ADCManager's conversion ready register flag
	byte Mask=ADManager->ConversionReadyRegister&(1<<OptionShiftRegister);
	if(Mask)
		return TRUE;
	else
		return FALSE;
}

/*For OsqarProtocol*/
void DifferentialADC::UpdateConversionBytesValues()
{
	ConversionHigh=(ChannelConversion>>8)&255;
	ConversionLow=ChannelConversion&255;
}

uint8_t * DifferentialADC::ReadConversionByteHAddress()
{
	return &ConversionHigh;
}

uint8_t * DifferentialADC::ReadConversionByteLAddress()
{
	return &ConversionLow;
}
